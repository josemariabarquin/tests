﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIRest_Prueba.DataDB;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIRest_Prueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadosController : ControllerBase
    {
        private readonly APIRESTPruebaContext context;

        public EstadosController(APIRESTPruebaContext context)
        {
            this.context = context;
        }

        // GET: api/<EstadosController>
        [HttpGet]
        public IEnumerable<Estado> Get()
        {
            //return new Estado[] { "value1", "value2" };
            return context.Estados.ToList();
        }

        // GET api/<EstadosController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            //return id.ToString();
            return $"value {id}";
        }

        // POST api/<EstadosController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EstadosController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EstadosController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
