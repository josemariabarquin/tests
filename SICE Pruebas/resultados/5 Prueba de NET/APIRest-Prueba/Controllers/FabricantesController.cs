﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIRest_Prueba.DataDB;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIRest_Prueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FabricantesController : ControllerBase
    {

        private readonly APIRESTPruebaContext context;

        public FabricantesController(APIRESTPruebaContext context)
        {
            this.context = context;
        }

        // GET: api/<FabricantesController>
        //[HttpGet]
        //public IEnumerable<Fabricante> Get()
        //{
        //    return context.Fabricantes.ToList();
        //}

        [HttpGet]
        public IEnumerable<Fabricante> Get(APIRESTPruebaContext context) => context.Fabricantes.ToList();

        // GET api/<FabricantesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<FabricantesController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<FabricantesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<FabricantesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
