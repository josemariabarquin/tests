using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIRest_Prueba
{

    /*
    Command: Lee Esquema de Base Datos a Entity Framework. Se usa herramienta consola de administracion de paquetes.
    Scaffold-DbContext "Server=ANTARES; Database=API-REST-Prueba; Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir DataDB
    
    Command: Actualiza Esquema de Base Datos a Entity Framework. Se usa herramienta consola de administracion de paquetes.
    Scaffold-DbContext "Server=ANTARES; Database=API-REST-Prueba; Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir DataDB -force
 */


    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
