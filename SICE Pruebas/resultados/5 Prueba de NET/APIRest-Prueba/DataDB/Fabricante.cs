﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIRest_Prueba.DataDB
{
    public partial class Fabricante
    {
        public Fabricante()
        {
            Terminals = new HashSet<Terminal>();
        }

        public int IdFab { get; set; }
        public string FabName { get; set; }
        public string FabDesc { get; set; }

        public virtual ICollection<Terminal> Terminals { get; set; }
    }
}
