﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIRest_Prueba.DataDB
{
    public partial class Estado
    {
        public Estado()
        {
            Terminals = new HashSet<Terminal>();
        }

        public int IdEstado { get; set; }
        public string EstadoNanme { get; set; }
        public string EstadoDesc { get; set; }

        public virtual ICollection<Terminal> Terminals { get; set; }
    }
}
