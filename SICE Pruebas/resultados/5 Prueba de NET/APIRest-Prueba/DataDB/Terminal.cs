﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIRest_Prueba.DataDB
{
    public partial class Terminal
    {
        public int IdTerminal { get; set; }
        public int IdFab { get; set; }
        public int IdEstado { get; set; }
        public DateTime FechaFabricacion { get; set; }
        public DateTime FechaEstado { get; set; }
        public string TerminalDesc { get; set; }
        public string TerminalName { get; set; }

        public virtual Estado IdEstadoNavigation { get; set; }
        public virtual Fabricante IdFabNavigation { get; set; }
    }
}
