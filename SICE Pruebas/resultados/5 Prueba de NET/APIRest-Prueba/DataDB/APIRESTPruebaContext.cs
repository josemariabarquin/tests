﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace APIRest_Prueba.DataDB
{
    public partial class APIRESTPruebaContext : DbContext
    {
        public APIRESTPruebaContext()
        {
        }

        public APIRESTPruebaContext(DbContextOptions<APIRESTPruebaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Estado> Estados { get; set; }
        public virtual DbSet<Fabricante> Fabricantes { get; set; }
        public virtual DbSet<Terminal> Terminals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=ANTARES; Database=API-REST-Prueba; Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estado>(entity =>
            {
                entity.HasKey(e => e.IdEstado);

                entity.ToTable("estado");

                entity.Property(e => e.IdEstado)
                    .ValueGeneratedNever()
                    .HasColumnName("id_estado");

                entity.Property(e => e.EstadoDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("estado_desc");

                entity.Property(e => e.EstadoNanme)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("estado_nanme");
            });

            modelBuilder.Entity<Fabricante>(entity =>
            {
                entity.HasKey(e => e.IdFab)
                    .HasName("PK_fabricantes");

                entity.ToTable("fabricante");

                entity.Property(e => e.IdFab)
                    .ValueGeneratedNever()
                    .HasColumnName("id_fab");

                entity.Property(e => e.FabDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("fab_desc");

                entity.Property(e => e.FabName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("fab_name");
            });

            modelBuilder.Entity<Terminal>(entity =>
            {
                entity.HasKey(e => e.IdTerminal)
                    .HasName("PK_terminales");

                entity.ToTable("terminal");

                entity.Property(e => e.IdTerminal)
                    .ValueGeneratedNever()
                    .HasColumnName("id_terminal");

                entity.Property(e => e.FechaEstado)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_estado");

                entity.Property(e => e.FechaFabricacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_fabricacion");

                entity.Property(e => e.IdEstado).HasColumnName("id_estado");

                entity.Property(e => e.IdFab).HasColumnName("id_fab");

                entity.Property(e => e.TerminalDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("terminal_desc");

                entity.Property(e => e.TerminalName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("terminal_name");

                entity.HasOne(d => d.IdEstadoNavigation)
                    .WithMany(p => p.Terminals)
                    .HasForeignKey(d => d.IdEstado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terminales_estado");

                entity.HasOne(d => d.IdFabNavigation)
                    .WithMany(p => p.Terminals)
                    .HasForeignKey(d => d.IdFab)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_terminales_fabricantes");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
