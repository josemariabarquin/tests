-- Display the second highest salary drawing employee details.

Select * FROM Employee where Salary =(SELECT MAX(salary) AS Salary FROM Employee 
	WHERE Salary <> (SELECT MAX(Salary) FROM Employee));



-- Find out the employees who earn greater than the average salary for their department

SELECT e.First_Name, e.Last_Name, e.Salary, e.Department_ID FROM Employee e
WHERE Salary > (SELECT avg(salary) FROM Employee WHERE e.Department_ID = Department_ID 
	group BY Department_ID);


