﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoLibrary;
using System.IO;

namespace ConsoleUI
{
    /// <summary>
    /// One of the most important services offered by Gemalto is the identification management. To perform that, 
    /// Gemalto scans identification documents (such as passports, Id cards or driver licenses) and retrieves the 
    /// personal information within them.
    /// 
    /// As a result of this process, we are trying to create an identification database, avoiding duplicate results 
    /// (sometimes the same document is scanned twice) and filtering those documents that are not legit.
    /// 
    /// We are trying to avoid those cases where the same person presents two different documents. We are considering that two documents refer to the same person if:
    ///         - The first name, last name, nationality and date of birth are the same
    /// We are considering that two scans refer to the same document if:
    ///         - The document number, issuing country and document type are the same.
    /// Remember, OCR is not an exact science and some typos can be included in the data extracted.Apart from that, some kind 
    /// of documents are not accepted.Your code must be able to handle the following situations:
    ///         - Expired documents should be avoided.All dates in the document are represented by YYMMDD (i.e.April 10th 2005 will be represent as 050410). Wrong dates should result in discarded documents(you can assume that all dates are from this century).
    ///         - Issuing country and nationality are represented using ICAO three-letters format.If the field has more than 3 letters all letters after the first three should be ignored. We only accept Spanish (ESP) documents and those for neighbor countries. Spain is surrounded by France (FRA), Portugal (POR), Andorra (AND) and Morocco (MOR).
    /// 
    /// We need this detection code to run quickly.
    /// 
    /// The input file will be large enough so that it will behoove you to make your code as fast as possible.That said, please remember 
    /// that this identification system is part of a larger system and one that might change over time, and we expect the structure of your 
    /// code to reflect that fact.
    /// 
    ///                             ===============================================================
    /// 
    /// INPUT:
    /// First line will contain an integer N denoting the number of records, followed by N lines with one record per line.
    /// Each record contains the following information separated by commas:
    ///     scan id (numeric)
    ///     Document Type (P|ID|DL)
    ///     Issuing country
    ///     Last Name
    ///     First Name
    ///     Document number(numeric)
    ///     Nationality
    ///     Date Of birth
    ///     Date
    ///     
    /// OUTPUT:
    ///     A single line of comma-separated discarded scan ids in ascending order
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {

            try
            {
                CheckDuplicatedPerson checkValidPerson = new CheckDuplicatedPerson();
                CheckDuplicatedScan checkValidScan = new CheckDuplicatedScan();
                CheckValidIssuingCountry checkValidIssuingCountry = new CheckValidIssuingCountry();
                CheckExpiredDoc checkExpiredDoc = new CheckExpiredDoc();
                CheckValidateBirthDates checkValidBirthDates = new CheckValidateBirthDates();


                // Obtiene IDs de registros que se refieren a la misma persona.
                checkValidPerson.readFile();
                string invalidPeople = checkValidPerson.duplicatedPeople;

                // Obtiene IDs de documentos scaneados mas de una vez
                checkValidScan.readFile();
                string invalidScan = checkValidScan.duplicatedScan;

                // Obtiene IDs de documentos no se han emitido en ESP, FRA, MOR, AND
                checkValidIssuingCountry.readFile();
                string invalidIssuingCountry = checkValidIssuingCountry.invalidIssuingCountry;

                // Obtiene IDs de documentos que han caducado
                checkExpiredDoc.readFile();
                string lapsedDoc = checkExpiredDoc.expiredDocs;

                // Obtiene IDs de documentos cuyas fechas no son correctas
                checkValidBirthDates.readFile();
                string invalidBirthDates = checkValidBirthDates.invalidDates;
                

                Console.Write(invalidPeople + invalidScan + invalidIssuingCountry + lapsedDoc + invalidBirthDates);
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }
    }
}
