﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace DemoLibrary
{
    public class CheckValidateBirthDates
    {
        public string invalidDates
        { get; set; }

        IDictionary<string, string> dataBirhtDate = new Dictionary<string, string>();

        public void readFile()
        {
            try
            {
                StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "sample_input.txt"));
                string line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    line = sr.ReadLine();

                    if (!String.IsNullOrEmpty(line))
                    {
                        string[] dataCol = line.Split(',');
                        dataBirhtDate.Add(dataCol[0], dataCol[7]);
                    }
                }

                CheckInValidDates(dataBirhtDate);

                //close the file
                sr.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally CheckValidatedBirthDates block.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CheckInValidDates(IDictionary<string, string> birthDates)
        {
            string wrongDate = string.Empty;
            foreach (KeyValuePair<string, string> entry in birthDates)
            {
                string dateValue = ("20" + entry.Value).Substring(0,4) + "/" + ("20" + entry.Value).Substring(4, 2) + "/" + ("20" + entry.Value).Substring(6, 2);

                // Si la fecha no es válida
                if (!ValidateDate(dateValue))
                {
                    invalidDates += entry.Key + ",";
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateValue"></param>
        /// <returns></returns>
        private Boolean ValidateDate(string dateValue)
        {
            DateTime d;
            //dateValue = ;
            return DateTime.TryParseExact(
                dateValue,
                "yyyy/MM/dd",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out d);
        }

    }
}





