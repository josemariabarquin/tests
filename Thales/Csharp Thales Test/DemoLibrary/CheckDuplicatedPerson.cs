﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DemoLibrary
{

    /// <summary>
    /// Check if two documents refer to the same person; in that case, the first name, last name, nationality and date of birth are the same.
    /// </summary>
    public class CheckDuplicatedPerson
    {
        public string duplicatedPeople
        { get; set; }

        IDictionary<string, string> dataPeople = new Dictionary<string, string>();

        /// <summary>
        /// Lee el fichero de entrada y recoge datos personales: First Name, Last Name, Nationality
        /// </summary>
        public void readFile()
        {
            try
            {

                StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "sample_input.txt"));
                string line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    line = sr.ReadLine();

                    if (!String.IsNullOrEmpty(line))
                    { 
                        string[] dataCol = line.Split(',');
                        dataPeople.Add(dataCol[0], dataCol[3] + dataCol[4] + dataCol[6] + dataCol[7]);
                    }
                }

                CheckData();

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally CheckDuplicatedPerson block.");
            }
        }

        /// <summary>
        /// Obtiene registros que se refieren a la misma persona, almacena sus keys en un array
        /// </summary>
        private void CheckData()
        {
            var duplicateValues = dataPeople.GroupBy(x => x.Value).Where(x => x.Count() > 1);

            foreach (var item in duplicateValues)
            {
                var matches = dataPeople.Where(pair => pair.Value == item.Key).Select(pair => pair.Key);
                ElementsDuplicated(matches.ToArray());
            }
        }

        /// <summary>
        /// Concatena keys de registros que se refieren a la misma persona.
        /// </summary>
        /// <param name="samePeople"></param>
        private void ElementsDuplicated(string [] samePeople)
        {
            for (int index=1; index < samePeople.Length; index++) 
            { duplicatedPeople += samePeople[index] + ","; }
        }
    }
}
