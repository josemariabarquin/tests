﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DemoLibrary
{

    /// <summary>
    /// Check if two scans refer to the same document in that case, the document number, 
    /// issuing country and document type are the same.
    /// </summary>
    public class CheckDuplicatedScan
    {
        public string duplicatedScan
        { get; set; }

        IDictionary<string, string> dataPeople = new Dictionary<string, string>();

        public void readFile()
        {
            try
            {
                StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "sample_input.txt"));
                string line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    line = sr.ReadLine();

                    if (!String.IsNullOrEmpty(line))
                    {
                        string[] dataCol = line.Split(',');
                        dataPeople.Add(dataCol[0], dataCol[1] + dataCol[2] + dataCol[5]);
                    }
                }

                CheckData();

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally CheckDuplicatedScan block.");
            }
        }


        /// <summary>
        /// Obtiene registros scaneados mas de una vez, almacena sus keys en un array
        /// </summary>
        private void CheckData()
        {
            var repetidosValues = dataPeople.GroupBy(x => x.Value).Where(x => x.Count() > 1);

            foreach (var item in repetidosValues)
            {
                var matches = dataPeople.Where(pair => pair.Value == item.Key).Select(pair => pair.Key);
                ElementsDuplicated(matches.ToArray());
            }
        }


        /// <summary>
        /// Concatena keys de documentos scaneados más de una vez.
        /// </summary>
        /// <param name="duplicatedPeople"></param>
        private void ElementsDuplicated(string[] duplicatedPeople)
        {
            for (int index = 1; index < duplicatedPeople.Length; index++)
            { duplicatedScan += duplicatedPeople[index] + ","; }
        }
    }
}
