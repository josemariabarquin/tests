﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace DemoLibrary
{
    /// <summary>
    /// Expired documents should be avoided. All dates in the document are represented 
    /// by YYMMDD (i.e. April 10th 2005 will be represent as 050410). Wrong dates should 
    /// result in discarded documents (you can assume that all dates are from this century).
    /// </summary>
    public class CheckExpiredDoc
    {
        public string expiredDocs
        { get; set; }

        public string invalidDates
        { get; set; }

        IDictionary<string, int> dataDateOfExpired = new Dictionary<string, int>();

        /// <summary>
        /// Lee fichero de entrada
        /// </summary>
        public void readFile()
        {
            try
            {
                StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "sample_input.txt"));
                string line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    line = sr.ReadLine();

                    if (!String.IsNullOrEmpty(line))
                    {
                        string[] dataCol = line.Split(',');
                        dataDateOfExpired.Add(dataCol[0], Int32.Parse(dataCol[8]));
                    }
                }

                CheckExpiredDate();
                CheckInValidDates(dataDateOfExpired);

                //close the file
                sr.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally CheckExpiredDoc block.");
            }
        }

        /// <summary>
        /// Extrae registros que tienen la fecha caducada
        /// </summary>
        private void CheckExpiredDate()
        {
            int fHoy = GetDate();
            var dateExpired = from x in dataDateOfExpired
                              where x.Value < fHoy
                              select x;

            DateExpired((dateExpired.Select(pair => pair.Key)).ToArray());
        }

        /// <summary>
        /// Concatena IDs de registros con fecha caducada y los asigna a una propiedad 
        /// </summary>
        /// <param name="dateExp"></param>
        private void DateExpired(string[] dateExp)
        {
            for (int index = 0; index < dateExp.Length; index++)
            { expiredDocs += dateExp[index] + ","; }
        }

        /// <summary>
        /// Transforma fecha del formato yy/MM/dd al yyyyMMdd
        /// </summary>
        private int GetDate()
        {
            DateTime now = DateTime.Today;
            string day = now.Day < 10 ? "0" + now.Day.ToString() : now.Day.ToString();
            string month = now.Month < 10 ? "0" + now.Month.ToString() : now.Month.ToString();
            string year = Int32.Parse(now.Year.ToString().Substring(2, 2)) < 10 ? "0" + now.Year.ToString().Substring(2, 2) : now.Year.ToString().Substring(2, 2);

            return (Int32.Parse(year + month + day)); ;
        }

        /// <summary>
        /// Obtiene fechas invalidas, concatena sus IDs y los asigna a una propiedad
        /// </summary>
        private void CheckInValidDates(IDictionary<string, int> dateExpired)
        {
            string wrongDate = string.Empty;
            foreach (KeyValuePair<string, int> entry in dateExpired)
            {
                string dateValue = ("20" + entry.Value).Substring(0, 4) + "/" + ("20" + entry.Value).Substring(4, 2) + "/" + ("20" + entry.Value).Substring(6, 2);

                // Si la fecha no es válida
                if (!ValidateDate(dateValue))
                {
                    invalidDates += entry.Key + ",";
                }
            }
        }

        /// <summary>
        /// Evalua si la fecha de entrada es valida
        /// </summary>
        /// <param name="dateValue"></param>
        /// <returns></returns>
        private Boolean ValidateDate(string dateValue)
        {
            DateTime d;
            //dateValue = ;
            return DateTime.TryParseExact(
                dateValue,
                "yyyy/MM/dd",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out d);
        }
    }
}
