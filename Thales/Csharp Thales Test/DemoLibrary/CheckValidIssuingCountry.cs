﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DemoLibrary
{

    /// <summary>
    /// Issuing country and nationality are represented using ICAO three-letters format. 
    /// If the field has more than 3 letters all letters after the first three should be ignored. 
    /// We only accept Spanish (ESP) documents and those for neighbor countries. 
    /// Spain is surrounded by France (FRA), Portugal (POR), Andorra (AND) and Morocco (MOR).
    /// </summary>
    public class CheckValidIssuingCountry
    {
        public string invalidIssuingCountry
        { get; set; }

        IDictionary<string, string> dataPeople = new Dictionary<string, string>();

        public void readFile()
        {

            try
            {
                StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "sample_input.txt"));
                string line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    line = sr.ReadLine();

                    if (!String.IsNullOrEmpty(line))
                    {
                        string[] dataCol = line.Split(',');
                        dataPeople.Add(dataCol[0], dataCol[2].Substring(0,3).ToUpper());
                    }
                }

                CheckData();

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally CheckValidIssuingCountry block.");
            }
        }

        /// <summary>
        /// Filtra paises no validos para la emision de documentos de identificacion
        /// </summary>
        private void CheckData()
        {
            var invaldIssuingCountries = from x in dataPeople
                    where !x.Value.Contains("AND") && !x.Value.Contains("ESP") && !x.Value.Contains("POR") && !x.Value.Contains("MOR") && !x.Value.Contains("FRA")
                    select x;

            InvalidScanIdDocuments((invaldIssuingCountries.Select(pair => pair.Key)).ToArray());
        }

       /// <summary>
       /// Concatena identificadores de scaneado de registros no validos
       /// </summary>
       /// <param name="invaldIssuingCountries"></param>
        private void InvalidScanIdDocuments(string[] invaldIssuingCountries)
        {
            for (int index = 0; index < invaldIssuingCountries.Length; index++)
            { invalidIssuingCountry += invaldIssuingCountries[index] + ","; }
        }
    }
}
