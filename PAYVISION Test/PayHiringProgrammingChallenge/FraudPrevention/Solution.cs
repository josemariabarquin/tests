﻿using System;
using System.Collections.Generic;
using System.IO;
using FraudPrevention;
using System.Linq;

class Solution
{
    static void Main(String[] args)
    {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */

        int counter = 0;
        string line;
        string fileName = "SampleInput.txt";

        System.IO.StreamReader file =
           new System.IO.StreamReader(Path.GetFullPath(fileName));

        int lines = Convert.ToInt16(file.ReadLine().Trim());

        // Lista con objetos de encargos
        List<Lines> lstOrders = new List<Lines>();

        while (counter < lines)
        {
            line = file.ReadLine();
            lstOrders.Add(new Lines(line.Split(',')));
            counter++;
        }
 
        file.Close();

        // Lista para fraudes
        List<string> lstFrauds = new List<string>();

        foreach (var x in lstOrders)
        {
            foreach (var y in lstOrders)
            {
                if ((y.orderId != x.orderId) && (y.dealId == x.dealId) && (y.emailAddress == x.emailAddress) && (y.creditCard != x.creditCard))
                {
                    lstFrauds.Add(x.orderId);
                    lstFrauds.Add(y.orderId);
                }

                if ((y.orderId != x.orderId) && (y.dealId == x.dealId) && (y.city == x.city) && (y.state == x.state) && (y.zipCode == x.zipCode) && (y.creditCard != x.creditCard))
                {
                    lstFrauds.Add(x.orderId);
                    lstFrauds.Add(y.orderId);
                }
            }
        }

        lstFrauds = lstFrauds.Distinct().ToList();
        string strFrauds = string.Empty;

        foreach (string order in lstFrauds)
            strFrauds += order + ",";

    
        Console.WriteLine(strFrauds);

        // Suspend the screen.
        Console.ReadLine();

    }
}
