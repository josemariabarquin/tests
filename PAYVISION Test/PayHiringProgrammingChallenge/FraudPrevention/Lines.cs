﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FraudPrevention
{
    public class Lines
    {
        //Order id (numeric)
        //Deal id (numeric)
        //Email address
        //Street address
        //City
        //State
        //Zip Code
        //Credit Card #

        #region properties
        public string orderId { get; set; }
        public string dealId { get; set; }
        public string emailAddress { get; set; }
        public string streetAddress { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string creditCard { get; set; }

        #endregion

        #region constructors
        public Lines(string[] aLine)
        {
            string userPart = string.Empty;

            // Order
            this.orderId = aLine[0];

            // Deal
            this.dealId = aLine[1];

            // Email Address
            string[] arrEmail = aLine[2].Split('@');
            userPart = (arrEmail[0].IndexOf('+') != -1) ? arrEmail[0].Substring(0, arrEmail[0].IndexOf('+')) : arrEmail[0];
            this.emailAddress = userPart.Replace(".", string.Empty) + '@' + arrEmail[1].ToLower();

            // Street Address
            string sAddress = aLine[3].ToLower().Replace("street", "st.");
            sAddress = sAddress.Replace("road", "rd.");
            this.streetAddress = sAddress;
            
            // City
            this.city = aLine[4].ToLower();

            // State
            string estado = aLine[5].ToLower().Replace("california", "ca");
            estado = aLine[5].ToLower().Replace("illinois", "il");
            estado = aLine[5].ToLower().Replace("new york", "ny");
            this.state = estado;
            
            // ZIP Code
            this.zipCode = aLine[6];

            // Credict Card
            this.creditCard = aLine[7];
        }

        #endregion

    }
}
